#+title:  How to Use the Math Theme with Org-mode


* Load =insert-latex-macros.el=

#+begin_example
M-x load-file org-templates/insert-latex-macros
#+end_example


* In your org document
#+begin_example
  #+INCLUDE: /<path/to/math>/org-templates/level-n.org
#+end_example
