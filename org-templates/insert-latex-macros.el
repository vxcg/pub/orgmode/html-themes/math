;;; Author:  Venkatesh Choppella <vxc@gitlab.com>, Pranav Vats <vats@disroot.org>
;;; From https://emacs.stackexchange.com/questions/54703/exporting-latex-commands-to-html-mathjax

;;; Provides a new language (latex-macros) for use with org-babel.
;;; The latex-macros source code blocks expand to appropriate html and latex.

;;; Mathjax supports the following commands for defining macros
;;; \def, \newcommand, \renewcommand, \newenvironment, \renewenvironment, and \let
;;; As per https://docs.mathjax.org/en/v3.2-latest/input/tex/macros.html


;;; =====
;;; Evaluate this buffer.
(require 'org)

(setq org-export-use-babel t)

;;; Only latex-macros should be allowed
;;; for evaluation without confirmation
;;; https://emacs.stackexchange.com/questions/2945/org-babel-eval-with-no-confirmation-is-explicit-eval-yes#3570
(defun ck/org-confirm-babel-evaluate (lang body)
  (not (string= lang "latex-macros")))

(setq org-confirm-babel-evaluate 'ck/org-confirm-babel-evaluate)


(defvar org-babel-default-header-args:latex-macros
  '((:results . "raw")
    (:exports . "results")
	(:eval . "t")))

(defun prefix-all-lines (pre body)
  (with-temp-buffer
    (insert body)
    (string-insert-rectangle (point-min) (point-max) pre)
    (buffer-string)))

(defun org-babel-execute:latex-macros (body _params)
  (concat
   (prefix-all-lines "#+LATEX_HEADER: " body)
   "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
   (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
   "\n#+HTML_HEAD_EXTRA: \\)</div>\n"))

(provide 'latex-macros)

